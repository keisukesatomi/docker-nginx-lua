FROM ubuntu:14.04

MAINTAINER CorGinia KeisukeSatomi "dumpweed0dexter@gmail.com"

RUN apt-get update && apt-get install -y build-essential wget git libpcre3 libpcre3-dev zlib1g zlib1g-dev libssl-dev

WORKDIR /root
RUN wget http://nginx.org/download/nginx-1.9.4.tar.gz && tar xvzf nginx-1.9.4.tar.gz

RUN git clone http://luajit.org/git/luajit-2.0.git
RUN cd luajit-2.0 && make && make install

WORKDIR /usr/local/src
RUN git clone https://github.com/simpl/ngx_devel_kit.git

RUN git clone https://github.com/openresty/lua-nginx-module.git

RUN git clone https://github.com/openresty/echo-nginx-module.git

RUN groupadd -g 500 nginx && useradd -g nginx -u 500 -s /sbin/nologin -d /var/www nginx

RUN cd /root/nginx-1.9.4 && ./configure --with-ld-opt=-Wl,-E,-rpath,/usr/local/lib --add-module=/usr/local/src/echo-nginx-module --add-module=/usr/local/src/ngx_devel_kit --add-module=/usr/local/src/lua-nginx-module --user=nginx --group=nginx --with-http_ssl_module --prefix=/usr/local/nginx-1.9.4 && make && make install

RUN ln -s /usr/local/nginx-1.9.4/sbin/nginx /usr/bin/nginx

EXPOSE 80 443

CMD ["nginx", "-g", "daemon off;"]
